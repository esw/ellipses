To build the project, install dependencies manually or use Nix shell:

    nix-shell

Then build the project with Meson:

    meson setup build --buildtype=plain
    meson compile --verbose -C build

Using `--buildtype=plain` causes Meson not to use any debug and
optimization flags when running the compiler. In this task, we set the
flags manually in `meson.build` (via `cpp_args`). Investigate how the
compiler options can be tuned.

Using `--verbose` prints commands used to build the project to the
terminal. Investigate that output.

And finally run the project:

    ./build/ellipse/find_ellipse -h
    ./build/ellipse/find_ellipse images/table.jpg 1000 no-gui

[cachix.org]: https://www.cachix.org/
[esw cache]: https://app.cachix.org/organization/ctu-esw/cache/esw#pull
