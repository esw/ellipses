let
  sources = import ./npins;
  pkgs = import sources.nixpkgs { };
in
with pkgs;
mkShell {
  packages = [
    pkg-config
    meson
    ninja
    boost
    (opencv.override { enableGtk3 = true; })

    hyperfine

    linuxPackages.perf
    hotspot

    valgrind
    libsForQt5.kcachegrind
    graphviz
  ];
}
